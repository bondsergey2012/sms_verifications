import React from 'react'
import axios from 'axios';
import domains from './../services/domains';
import notify from './../services/notify'

class PhoneVerify extends React.Component {

  constructor() {
    super();
    this.state = {
      phone: '',
      info: false,
      loadPhone: false,
    }
  }

  onChange(e) {
    const phone = e.target.value;
    this.setState({phone});
  }

  send() {
    this.setState({loadPhone: true});
    axios.get(`${domains.domain}/api/v1/verify_phone/${this.state.phone}`)
      .then((result) => {
        const info = result.data;
        this.setState({info, loadPhone: false});
      })
      .catch(() => {
        notify.error('Error! Something went wrong');
        this.setState({loadPhone: false});
      })
  }

  refresh() {
    this.setState({load: true});
    axios.get(`${domains.domain}/api/v1/get_lead?_id=${this.state.info._id}`)
      .then((result) => {
        const info = result.data;
        this.setState({info, load: false});
      })
      .catch(() => {
        notify.error('Error! Something went wrong');
        this.setState({load: false});
      });
  }

  render() {
    const {info, load, loadPhone} = this.state;
    return (
      <div>
        <div className="page-title ng-scope"><h3>Phone Verify</h3></div>
        <div id="main-wrapper" className="ng-scope afade">
          <div className="row">
            <div className="col-xs-12">
              <div className='panel info-box panel-white'>
                <div className='panel-body'>
                  <div className="phone-panel">
                    <label>Enter phone number</label>
                    <input onChange={(e) => this.onChange(e)} disabled={loadPhone}/>
                  </div>
                  <button onClick={() => this.send()} className="btn btn-primary" disabled={loadPhone}>Send verify
                  </button>
                  {info &&
                  <div className="phone-info">
                    <hr className="mt-0"/>
                    <div className="phone-panel mb-15">
                      <h4>File Details</h4>
                      <button className="btn btn-primary" onClick={() => this.refresh()} disabled={load}>Update info
                      </button>
                    </div>
                    <div className="loan-details-title">
                      <b>ID:</b> {info._id}
                    </div>
                    <div className="loan-details-title">
                      <b>Code: </b> {info.code}
                    </div>
                    <div className="loan-details-title">
                      <b>Status: </b> {info.status}
                    </div>
                  </div>}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PhoneVerify;
