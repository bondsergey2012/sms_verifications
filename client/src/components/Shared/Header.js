import React from 'react'
import {Link, withRouter} from 'react-router-dom'
import user from '../services/user'
let className = 'small-sidebar';
let $ = window.$;

class Header extends React.Component {

  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
  }

  componentDidMount() {
    document.body.classList.toggle(className, localStorage.getItem('sidebar') === 'small');
    document.onclick = function (e) {
      e.stopPropagation();
      let container = $(".dropdown-mobile");
      let container2 = $(".sidebar-pusher");
      if (container.has(e.target).length === 0 && container2.has(e.target).length === 0) {
        $('.dropdown-mobile').removeClass('active');
        $('.dropdown__content').removeClass('open');
      }
    }
  }

  toggle() {
    let value = document.body.classList.contains(className);
    document.body.classList.toggle(className, !value);
    localStorage.setItem('sidebar', value ? 'full' : 'small')
  }

  openDrDn() {
    let hidden = $('.dropdown-mobile');
    if (hidden.hasClass('active')){
      hidden.removeClass('active');
      $('.dropdown__content').removeClass('open');
    } else {
      hidden.addClass('active');
      $('.dropdown__content').addClass('open');
    }
  }

  render() {
    return <div>
      <div className="navbar">
        <div className="navbar-inner">
          {/*<i className="fa fa-bars pull-left visible-xs"></i>*/}
          <div className="sidebar-pusher">
            <a className="waves-effect waves-button waves-classic push-sidebar" onClick={this.openDrDn.bind(this)}>
              <i className="fa fa-bars"></i>
            </a>
          </div>
          <div className="logo-box">
            <Link to="/app" className="logo-text">
              <span className="full-logo-text">SMS</span>
              <span className="small-logo-text">S</span>
            </Link>
            {/*<Link to="/admin" className="logo-text">F</Link>*/}
          </div>
          <div className="dropdown dropdown-mobile">
            <div className="dropdown__content">
              <ul className="dropdown-menu dropdown-menu--header-mobile">
                <li><a href="/upload">Upload</a></li>
                <li><a href="/files">Files</a></li>
                <li><a href="/phone">Phone</a></li>
              </ul>
            </div>
          </div>
          <div className="topmenu-outer">
            <div className="top-menu">
              <ul className="nav navbar-nav navbar-left">
                <li>
                  <a onClick={this.toggle}
                     className="waves-effect waves-button waves-classNameic sidebar-toggle">
                    <i className="fa fa-bars"></i>
                  </a>
                </li>
              </ul>
              <ul className="nav navbar-nav navbar-right">
                <li className="dropdown">
                  <a className="dropdown-toggle waves-effect waves-button waves-classNameic main-ava"
                     data-toggle="dropdown">

                    <div className="pull-right">

                      <div>
                        <span className="pull-left">{user.get_public_name()} <i className="fa ml-5 fa-sign-out"></i></span>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </a>
                  <ul className="dropdown-menu dropdown-list" role="menu">
                    <li role="presentation"><a><i className="fa fa-user"></i>Profile</a></li>
                    <li role="presentation"><a><i className="fa fa-sign-out m-r-xs"></i>Log out</a></li>
                  </ul>
                </li>
                <li className="dropdown">
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  }
}

export default withRouter(Header)
