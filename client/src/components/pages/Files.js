import React from 'react'
import axios from 'axios';
import domains from './../services/domains';
import m from './../services/m';
import Loading from './../Shared/Loading';
import ModalLead from './../Shared/ModalLead';
import ReactPaginate from 'react-paginate';

class Files extends React.Component {

  constructor() {
    super();
    this.state = {
      files: [],
      isLoading: true,
      openModal: false,
      selectedFile: {},
      pagination: {
        perPage: 5,
        total: 0,
        page: 0
      }
    }
  }

  componentWillMount() {
    this.getAllFiles();
  }

  openModal(file) {
    this.setState({openModal: true, selectedFile: file});
  }

  closeModal() {
    this.setState({openModal: false, selectedFile: {}});
  }

  updateFile(file) {
    let {selectedFile, files} = this.state;
      files.forEach((item) => {
        if(item._id === selectedFile._id) {
          if(!file) {
            item.isSend = true;
          } else {
            item.leads = file;
          }
        }
      });
      this.setState({files});
  }

  getAllFiles() {
    this.setState({isLoading: true});
    const { pagination } = this.state;
    axios.get(`${domains.domain}/api/v1/get_all_files?page=${pagination.page}&perPage=${pagination.perPage}`)
      .then((result) => {
        let files = result.data.files;
        pagination.total = result.data.count;
        this.setState({isLoading: false, pagination, files});
      });
  }

  setPage(e) {
    const {pagination} = this.state;
    pagination.page = e.selected;
    this.setState({pagination});
    this.getAllFiles();
  }

  updatePageCount(e) {
    const {pagination} = this.state;
    pagination.perPage = e.target.value;
    pagination.page = 0;
    this.setState({pagination});
    this.getAllFiles();
  }

  render() {
    const {isLoading, files, selectedFile, openModal, pagination} = this.state;
    return (
      <div>
        <ModalLead isOpen={openModal} file={selectedFile} closeModal={() => this.closeModal()} updateFile={(file) => this.updateFile(file)}/>
        <div className="page-title ng-scope"><h3>Files</h3></div>
        <div id="main-wrapper" className="ng-scope afade">
          <div className="row">
            <div className="col-xs-12">
              <div className='panel info-box panel-white'>
                <div className='panel-body'>
                  <div className="dataTables_length mb-10" id="example2_length">
                    <div className="pull-left">
                      <small className="ib">Show</small>
                      <div className="ib w-paging">
                        <select className="form-control input-sm ib pagination-select" value={pagination.perPage}
                                onChange={(e) => this.updatePageCount(e)}>
                          <option value="5">5</option>
                          <option value="10">10</option>
                          <option value="25">25</option>
                          <option value="50">50</option>
                        </select>
                      </div>
                      <small className="ib">entries</small>
                    </div>
                    <div className="clearfix"></div>
                  </div>
                  <table className="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>File Name</th>
                      <th>Leads count</th>
                      <th>Send verify</th>
                      <th>Created</th>
                      <th>Updated</th>
                    </tr>
                    </thead>
                    <tbody>
                    {!!files.length && files.length > 0 && files.map((item, i) => {
                      return <tr key={'data-' + i} onClick={() => this.openModal(item)}>
                        <td>{item.originName}</td>
                        <td>{item.leads.length}</td>
                        <td>{item.isSend ? 'Verify was sent' : 'Verify was not sent'}</td>
                        <td>{m.date(item.created_at)}</td>
                        <td>{m.date(item.updated_at)}</td>
                      </tr>
                    })
                    }
                    {!files.length && <tr>
                      <td colSpan="100%" className={`text-center nothing-found ${isLoading && 'relative-td'}`}>
                        {isLoading && <Loading/>}
                        {!isLoading && <span>Nothing Found ...</span>}
                      </td>
                    </tr>}
                    </tbody>
                  </table>
                  <div className="pull-left">
                    <small>Showing {(pagination.page * pagination.perPage) + 1}
                      <span> to </span> {Math.min((pagination.page + 1) * pagination.perPage, pagination.total)}
                      <span> of</span> {pagination.total} entries
                    </small>
                  </div>
                  <div className="pull-right">
                    <ReactPaginate previousLabel={"previous"}
                                   nextLabel={"next"}
                                   breakLabel={<a>...</a>}
                                   breakClassName={"break-me"}
                                   pageCount={Math.ceil(pagination.total / pagination.perPage)}
                                   forcePage={pagination.page}
                                   marginPagesDisplayed={2}
                                   pageRangeDisplayed={5}
                                   onPageChange={(e) => this.setPage(e)}
                                   containerClassName={"pagination"}
                                   subContainerClassName={"pages pagination"}
                                   activeClassName={"active"}/>
                  </div>
                  <div className="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Files;
