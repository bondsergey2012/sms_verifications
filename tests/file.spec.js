const chai = require('chai');
const expect = chai.expect;
const spies = require('chai-spies');
const file = require('../microservices/file');
const Lead = require('../models/lead');
const File = require('../models/file');
const chaiHttp = require('chai-http');
process.env.PORT = '8080';
const server = require('../server');
const should = chai.should();

chai.use(spies);
chai.use(chaiHttp);

chai.use(spies);

describe('File', () => {
  beforeEach((done) => {
    File.remove({}, () => {
      Lead.remove({}, () => {
        done();
      });
    });
  });

  afterEach(() => {
    server.close();
  });

  it('should be defined', () => {
    expect(file).to.exist;
    expect(typeof file.getFile).to.equal('function');
    expect(typeof file.saveFile).to.equal('function');
  });

  it('should get a empty array', () => {
    expect(Array.isArray(file.readFile())).to.equal(true);
    expect(file.readFile().length).to.equal(0);
  });

  it('should get a not empty array', () => {
    expect(Array.isArray(file.readFile('test.txt'))).to.equal(true);
    expect(file.readFile('tests/test.txt').length).to.equal(3);
    expect(file.readFile('tests/test.txt')[0]).to.equal('+111111111');
  });

  it('should get a not empty array', () => {
    expect(Array.isArray(file.readFile('test.txt'))).to.equal(true);
    expect(file.readFile('tests/test.txt').length).to.equal(3);
    expect(file.readFile('tests/test.txt')[0]).to.equal('+111111111');
  });

  it('should get a error', (done) => {
    chai.request(server)
      .post('/api/v1/upload_file')
      .end((err, res) => {
        res.should.have.status(500);
        done();
      })
  });

  it('should get a error', (done) => {
    chai.request(server)
      .get('/api/v1/get_file')
      .end((err, res) => {
        res.should.have.status(500);
        done();
      })
  });

  it('should get same _id`s', (done) => {
    const req = {
      file: {
        fieldname: 'selectedFile',
        originalname: 'test.txt',
        encoding: '7bit',
        mimetype: 'text/plain',
        destination: './uploads',
        filename: 'test.txt',
        path: 'tests/test.txt',
        size: 37
      }
    };
    file.saveFile(req, {
      send: (data) => {
        expect(Array.isArray(data)).to.equal(true);
        expect(data.length).to.equal(1);
        File.find((err, file) => {
          expect(Array.isArray(file)).to.equal(true);
          expect(file.length).to.equal(1);
          expect(file[0]._id.toString()).to.equal(data[0]._id.toString());
          done();
        })
      }
    })
  });

  it('should get a not empty array', (done) => {
    const req = {
      file: {
        fieldname: 'selectedFile',
        originalname: 'test.txt',
        encoding: '7bit',
        mimetype: 'text/plain',
        destination: './uploads',
        filename: 'test.txt',
        path: 'tests/test.txt',
        size: 37
      }
    };
    file.saveFile(req, {
      send: () => {
        Lead.find((err, leads) => {
          expect(Array.isArray(leads)).to.equal(true);
          expect(leads.length).to.equal(3);
          done();
        })
      }
    })
  });

  it('should get status 200 with empty page numbers', (done) => {
    chai.request(server)
      .get('/api/v1/get_all_files')
      .end((err, res) => {
        res.should.have.status(500);
        done();
      })
  });

  it('should get status 200 with error page numbers', (done) => {
    chai.request(server)
      .get('/api/v1/get_all_files?page=www&perPage=ee')
      .end((err, res) => {
        res.should.have.status(500);
        done();
      })
  });

  it('should get status 200 with current page numbers', (done) => {
    chai.request(server)
      .get('/api/v1/get_all_files?page=1&perPage=1')
      .end((err, res) => {
        res.should.have.status(200);
        done();
      })
  })
});
