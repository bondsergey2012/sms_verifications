const chai = require('chai');
const expect = chai.expect;
const spies = require('chai-spies');
const sms = require('../microservices/sms');
const file = require('../microservices/file');
const Lead = require('../models/lead');
const chaiHttp = require('chai-http');
process.env.PORT = '8080';
const server = require('../server');

chai.use(spies);
chai.use(chaiHttp);

describe('lead', () => {
  beforeEach((done) => {
    Lead.remove({}, () => {
      done();
    });
  });
  afterEach(() => {
    server.close();
  });

  it('should be defined', () => {
    expect(true).to.equal(true);
    expect(sms).to.exist;
    expect(typeof sms.sendSms).to.equal('function');
    expect(typeof sms.confirmSms).to.equal('function');
    expect(typeof sms.sendSmsFromFile).to.equal('function');
    expect(typeof sms.statusSms).to.equal('function');
  });

  it('should add a Lead with status error', (done) => {
    const req = {
      params: {
        phone: 'qweqweqwe'
      }
    };
    sms.sendSms(req, {
      send: () => {
        Lead.find(function (err, data) {
          expect(Array.isArray(data)).to.equal(true);
          expect(data.length).to.equal(1);
          expect(data[0].status).to.equal('error');
          done();
        });
      }
    });
  });

  it('should add a Lead with status sent', (done) => {
    const req = {
      params: {
        phone: '+79885488529'
      }
    };
    sms.sendSms(req, {
      send: () => {
        Lead.find(function (err, data) {
          expect(Array.isArray(data)).to.equal(true);
          expect(data.length).to.equal(1);
          expect(data[0].status).to.equal('sent');
          done();
        });
      }
    });
  });

  it('should add a Lead', (done) => {
    chai.request(server)
      .get('/api/v1/verify_phone/qwerty')
      .end((err, res) => {
        res.should.have.status(200);
        Lead.find((err, data) => {
          expect(Array.isArray(data)).to.equal(true);
          expect(data.length).to.equal(1);
          done();
        });
      });
  });

  it('should get status 500 with empty file_id', (done) => {
    chai.request(server)
      .get('/api/v1/send_sms')
      .end((err, res) => {
        res.should.have.status(500);
        done();
      });
  });

  it('should get status 200 and not empty array', (done) => {
    const req = {
      file: {
        fieldname: 'selectedFile',
        originalname: 'test.txt',
        encoding: '7bit',
        mimetype: 'text/plain',
        destination: './uploads',
        filename: 'test.txt',
        path: 'tests/test.txt',
        size: 37
      }
    };
    file.saveFile(req, {
      send: (data) => {
        chai.request(server)
          .get('/api/v1/send_sms?file_id=' + data[0]._id)
          .end((err, res) => {
            res.should.have.status(200);
            Lead.find((err, leads) => {
              expect(Array.isArray(leads)).to.equal(true);
              expect(leads.length).to.equal(3);
            });
            done();
          });
      }
    })
  });

  it('should get status 500 with error file_id', (done) => {
    chai.request(server)
      .get('/api/v1/send_sms?file_id=1111111111111111111111')
      .end((err, res) => {
        res.should.have.status(500);
        done();
      });
  });

  it('should get status error', (done) => {
    const req = {
      file: {
        fieldname: 'selectedFile',
        originalname: 'error_test.txt',
        encoding: '7bit',
        mimetype: 'text/plain',
        destination: './uploads',
        filename: 'error_test.txt',
        path: 'tests/error_test.txt',
        size: 34
      }
    };
    file.saveFile(req, {
      send: (data) => {
        chai.request(server)
          .get('/api/v1/send_sms?file_id=' + data[0]._id)
          .end((err, res) => {
            setTimeout(() => {
              Lead.find((err, leads) => {
                expect(Array.isArray(leads)).to.equal(true);
                leads.forEach((t) => {
                  expect(t.status).to.equal('error');
                });
                done();
              })
            }, 1000)
          });
      }
    })
  });

  it('should get status 500 sms with empty code', (done) => {
    chai.request(server)
      .get('/confirm')
      .end((err, res)=>{
        res.should.have.status(500);
        done();
      })
  });

  it('should get status 500 sms with error code', (done) => {
    chai.request(server)
      .get('/confirm?code=1111111111111111')
      .end((err, res)=>{
        res.should.have.status(500);
        done();
      })
  });

  it('should get status 200 sms with current code', (done) => {
    const lead = new Lead({phone: 1, status: 'new', code: '123'});
    lead.save((error, lead) => {
      chai.request(server)
        .get('/confirm?code=' + lead.code)
        .end((err, res) => {
          res.should.have.status(200);
          Lead.findOne({_id: lead._id}, (err, data) => {
            expect(data.status).to.equal('confirmed');
            done();
          });
        })
    })
  });

  it('should get status 200 with current _id lead', (done) => {
    const lead = new Lead({phone: 1, status: 'new', code: '123'});
    lead.save((error, lead) => {
      chai.request(server)
        .get('/api/v1/get_lead?_id=' + lead._id)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        })
    })
  });

  it('should get status 500 with error _id lead', (done) => {
      chai.request(server)
        .get('/api/v1/get_lead?_id=11111111')
        .end((err, res) => {
          res.should.have.status(500);
          done();
        })
  });

  it('should get status 500 with empty _id lead', (done) => {
    chai.request(server)
      .get('/api/v1/get_lead')
      .end((err, res) => {
        res.should.have.status(500);
        done();
      })
  });

});
