var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var LeadSchema   = new Schema({
    phone: {
        type : String,
        required : true
    },
    msid: String,
    messageStatus: String,
    confirmed: Boolean,
    code: String,
    accountSid: String,
    replyMsg: String,
    status: String,
    response: String
});

module.exports = mongoose.model('Lead', LeadSchema);