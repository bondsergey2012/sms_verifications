const m = {
  date(v) {
    let date = new Date(v);
    let time = date.getTime();
    if (!(time > 0) && !(time < 0)) {
      return '-'
    }
    let mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    // let mS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

    return [date.getDate(), ' ', mL[date.getMonth()], ', ', date.getFullYear()].join('')
  }
};

export default m;
