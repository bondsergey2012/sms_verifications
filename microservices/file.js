const fs = require('fs');
const async = require('async');
const File = require('../models/file');
const Lead = require('../models/lead');

const getFile = (file) => {
  if (!fs.existsSync(file)) {
    return [];
  } else {
    let arr = fs.readFileSync(file, 'utf-8').split("\n");
    return arr.filter(item => item.trim() !== "");
  }
};

const isPhoneNumber = (num) => {
  // TODO
  return !!num;
};

exports.saveFile = (req, res) => {
  if(!req.file) return res.status(500).send({message: 'Error, file is not added!'});
  const numbers = getFile(req.file.path);
  res.leadsId = [];

  async.waterfall([(cb) => {
    if (numbers.length === 0) cb();
    numbers.forEach((i, iter) => {
      const lead = new Lead({phone: i, status: 'new'});
      lead.save((error, lead) => {
        res.leadsId.push(lead._id);
        if (iter === numbers.length - 1) {
          cb();
        }
      });
    });
  }, (cb) => {
    const file = new File({path: req.file.path, leads: res.leadsId, originName: req.file.originalname, isSend: false});
    file.save((err, files) => {
      File.find({_id: files._id})
        .populate({
          path: 'leads',
          model: Lead
        })
        .exec((err, data) => {
          if (err) res.send(err);
          res.send(data);
          cb();
        });
    });
  }]);
};

exports.getFile = (req, res) => {
  if (!req.query || !req.query.file_id) return res.status(500).send('File doesn\'t exist');
  File.findOne({_id: req.query.file_id})
    .populate({
      path: 'leads',
      model: Lead
    })
    .exec((err, data) => {
      res.send(data);
    });
};

exports.getAllFiles = (req, res) => {
  if (req.query && (!req.query.perPage  || !req.query.page)){
    return res.status(500).send('error');
  }
  let {perPage, page} = req.query;
  perPage = Number(perPage);
  page = Number(page);
  if(Number.isNaN(page) || Number.isNaN(perPage)){
    return res.status(500).send('error');
  }
  File.find({})
    .skip(perPage * page)
    .limit(perPage)
    .populate({
      path: 'leads',
      model: Lead,
    })
    .exec((err, data) => {
    if (!err){
      File.count({}, (err, count) => {
        res.send({files: data, count});
      });
    }else{
      res.status(500).send('error');
    }
    });
};

exports.readFile = getFile;
