# SMS verifications

**Run project**

1. Start server run  ``` npm run start```
2. Start client  go to directory _client_ ```cd client ``` and run
```PORT=3001 npm start```

**Run test**

 Run command 
``` npm test```

**Description of requests**

GET on _api/v1/verify_phone/:phone_: Sending verification by number from  parameters

Example response:

```{
     "__v": 0,
     "phone": "+79885488529",
     "msid": "SM80255dc57cbd485d85e29f80a7f6eb15",
     "code": "guo546fb8",
     "messageStatus": "queued",
     "accountSid": "ACd1f61302338b164d68868c77f3363b4c",
     "response": null,
     "status": "sent",
     "_id": "5a579e0908563b144f9e5e0d"
   }
 ```

GET on _/confirm_: Confirmation of verification

GET on _api/v1/send_sms?file_id={}_: Sending verification to numbers specified in file

```
{"message":"Verification started!"}
```

GET on _api/v1/get_file_: Return file by id

Example response:

```[{
  "_id": "5a579af02628eb1077d8f0f7",
  "path": "uploads/f9ab874a-26f8-48a1-bd1d-02ec6b34dff7.txt",
  "__v": 0,
  "leads": [{"_id": "5a579af02628eb1077d8f0f6", "phone": "123123123", "status": "new", "__v": 0}]
}]
 ```

POST on _api/v1/upload_file_: Load file on server and added file in database

Example response:

```[{
  "_id": "5a579af02628eb1077d8f0f7",
  "path": "uploads/f9ab874a-26f8-48a1-bd1d-02ec6b34dff7.txt",
  "__v": 0,
  "leads": [{"_id": "5a579af02628eb1077d8f0f6", "phone": "123123123", "status": "new", "__v": 0}]
}]
```