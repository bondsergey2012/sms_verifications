var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var FileSchema   = new Schema({
  path: {
    type : String,
    required : true
  },
  isSend: Boolean,
  originName: String,
  created_at: { type: Date }
, updated_at: { type: Date },
  leads: [{
    type: String,
    ref: 'Lead'
  }]
});

FileSchema.pre('save', function(next){
  let now = new Date();
  this.updated_at = now;
  if ( !this.created_at ) {
    this.created_at = now;
  }
  next();
});

module.exports = mongoose.model('File',FileSchema);