const express = require('express');
const router = express.Router();
const upload = require('../config/upload');
const file = require('../microservices/file');
const sms = require('../microservices/sms');

router.get('/api/v1/verify_phone/:phone', sms.sendSms);

router.get('/confirm', sms.confirmSms);

router.post('/api/v1/upload_file', upload.single('selectedFile'), file.saveFile);

router.get('/api/v1/send_sms', sms.sendSmsFromFile);

router.get('/api/v1/get_file',file.getFile);

router.get('/api/v1/get_all_files', file.getAllFiles);

router.get('/api/v1/get_lead', sms.getLead);

module.exports = router;
