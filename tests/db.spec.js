const chai = require('chai');
const expect = chai.expect;
const file = require('../models/file');
const lead = require('../models/lead');

describe('db', () => {
  it('should be invalid if path is empty', (done) => {
    let m = new file();
    m.validate((err) => {
      expect(err.errors.path).to.exist;
      done();
    })
  });

  it('should be invalid if phone is empty', (done) => {
    let m = new lead();
    m.validate((err) => {
      expect(err.errors.phone).to.exist;
      done();
    })
  })
});

