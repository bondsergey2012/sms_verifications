const twilio = require('twilio');
const accountSid = 'ACd1f61302338b164d68868c77f3363b4c';
const authToken = '4a6120f731c35c8f67f9da2c7e40f82d';
const client = new twilio(accountSid, authToken);
const request = require('request');


const domains = require('../config/domains');
const Lead = require('../models/lead');
const File = require('../models/file');


const createSms = (_id, phone, res) => {
  const code = Math.random().toString(36).substr(2, 9);
  if (_id) {
    Lead.findOne({_id}).exec((err, lead) => {
      sendSms(lead.phone, code, (err, message) => {
        let status = 'sent';
        if (err || !message) {
          status = 'error';
          lead.response = err;
        }
        message = message || {};
        lead.msid = message.sid || null;
        lead.code = code || null;
        lead.messageStatus = message.status || null;
        lead.accountSid = message.accountSid || null;
        lead.status = status;
        lead.save(function (error) {
        });
      });
    })
  } else {
    sendSms(phone, code, (err, message) => {
      let status = 'sent';
      let response = null;
      if (err || !message) {
        status = 'error';
        response = err;
        message = {};
      }
      const lead = new Lead({
        phone: phone,
        msid: message.sid,
        code: code,
        messageStatus: message.status,
        accountSid: message.accountSid,
        response, status
      });
      lead.save(() => {
        res.send(lead);
      });
    });
  }
};

const sendSms = (phone, code, cb) => {
  client.messages.create(
    {
      to: phone,
      from: '+14154624571',
      body: `Open to confirm: ${domains.domain}/confirm/?code=${code}`,
      // statusCallback: `${domains.domain}/status_cb`,
      provideFeedback: true
    }, (err, message) => {
      cb(err, message);
    });
};

exports.sendSms = (req, res) => {
  const phone = req.params.phone;
  createSms(false, phone, res)
};

exports.sendSmsFromFile = (req, res) => {
  if (!req.query.file_id) return res.status(500).send('File doesn\'t exist');
  File.findOne({_id: req.query.file_id})
    .exec((err, data) => {
      if (err) res.status(500).send('Error');
      else {
        data.leads.forEach((item) => {
          createSms(item, req.query.file_id);
        });
        data.isSend = true;
        data.save(() => {
          res.status(200).send({message: 'Verification started!', isSend: true});
        });
      }
    });
};

exports.confirmSms = (req, res) => {
  if (!req.query.code) return res.status(500).send('Code doesn\'t exist');
  Lead.findOne({code: req.query.code}).exec((err, lead) => {
    if (err || !lead) return res.status(500).send('Code is incorrect');
    request({
      url: `https://${accountSid}:${authToken}@api.twilio.com/2010-04-01/Accounts/${lead.accountSid}/Messages/${lead.msid}/Feedback`,
      method: 'POST',
      form: {
        Outcome: "Confirmed"
      }
    }, () => {
      lead.confirmed = true;
      lead.status = 'confirmed';
      lead.save(() => {
        res.status(200).render('confirmed', {lead: lead});
      });
    })
  })
};

exports.statusSms = (req, res) => {
  const messageSid = req.body.MessageSid;
  const messageStatus = req.body.MessageStatus;
  Lead.findOne({msid: messageSid}).exec((err, lead) => {
    lead.messageStatus = messageStatus;
    lead.save()
  });
  res.send(200);
};

exports.getLeads = (req, res) => {
  Lead.find(function (err, data) {
    if(err) res.send(err);
    res.send(data);
  });
};

exports.getLead = (req, res) => {
  if (req.query && !req.query._id){
    return res.status(500).send('error')
  }
  const _id = req.query._id;
  Lead.findById(_id)
    .exec((err, lead) => {
      if (!err) {
        res.status(200).send(lead)
      } else {
        res.status(500).send('error')
      }
    });
};

exports.testFunctions = {
  sendSms,
  createSms
};
